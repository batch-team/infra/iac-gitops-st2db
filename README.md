# iac-gitops-st2db

Stackstorm DB gitops repository

Installs Mongo, Redis, RabbitMQ dependencies for stackstorm and exposes them behind ingress.

These are meant to be used in the puppet deployments of stackstorm, while fetching the secrets from teigi. 

## Details

* It is meant to be deployed by clusters in the OpenStack project `IT-Batch Infrastructure - Container` as the SOPS master key is located there.
* It deploys an ingress controller so the default one from OpenStack Magnum wouldn't be needed.
* To enable the ingress controller, you need to tag nodes with the role `role=custom-ingress`. Like this: `kubectl label node fernandl-db-pvyaf6jugxlx-node-0  role=custom-ingress`.
* The ingress deployed uses a LoadBalancer by default, this fact and the automated loadbalancer logic in our Cloud ([Kubernetes Service Type Loadbalancer](https://clouddocs.web.cern.ch/networking/load_balancing.html#kubernetes-service-type-loadbalancer)) makes the creation automatic.
* After the deployment, you should set the description of the loadbalancer created to give it a friendly DNS name: `openstack loadbalancer set --description <your-friendly-name> kube_service_<something-something>_stackstorm-ingress-nginx-controller`. This will make your loadbalancer available with the name `<your-friendly-name>.cern.ch`.
* Currently, the test and prod releases are available at `st2db-test.cern.ch` and `st2db-prod.cern.ch` respectively.
* It opens some TCP ports for redis & mongo. 
  * You can test redis with a session like this:
    ```
    $ ./redis-cli -h <your-friendly-name>.cern.ch
    137.138.6.17:6379> AUTH <the-password>
    OK
    137.138.6.17:6379> PING
    PONG
    ```
  * Or mongo:
    ```
    $ ./mongosh --username root "mongodb://<your-friendly-name>.cern.ch:27017/"
    Enter password: **********
    Current Mongosh Log ID:
    Connecting to:    mongodb://<your-friendly-name>.cern.ch:27017/?directConnection=true
    Using MongoDB:      4.4.4
    Using Mongosh Beta: 0.9.0
    
    For mongosh info see: https://docs.mongodb.com/mongodb-shell/
    
    > 
    ```

## Deployment

You need to deploy both the helm operator and flux itself:
```bash
$ helm upgrade -i helm-operator fluxcd/helm-operator --namespace flux \
    --values helm-operator-values.yaml --set allowNamespace=<test/prod>

$ helm upgrade -i flux fluxcd/flux --namespace flux \
    --version 1.3.0 \
    --set git.url=https://gitlab.cern.ch/batch-team/infra/iac-gitops-st2db \
    --set git.branch=<dev/master> \
    --values flux-values.yaml
```

The files *helm-operator-values.yaml* and *flux-values.yaml* contain the
configuration for each of the components. You can change these as required.

Check the logs to see the multiple releases are being picked up:
```
$ kubectl -n flux logs -f deployment.apps/helm-operator
$ kubectl -n flux logs -f deployment.apps/flux
```
